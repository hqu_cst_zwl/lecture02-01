package cn.edu.hqu.cst.lecture02_01.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
@Controller
public class WebController {
	//响应到“/page”的GET请求
	@GetMapping("/page")
	public String getPage() {
		//返回index视图，如index.jsp，index.html
		return "index";
	}
	
	@GetMapping("/data")
	@ResponseBody
	public List<String> getData(){
		//直接返回书籍列表
		return Arrays.asList("The Great Gatsby", "Moby Dick", "War and Peace");
	}

}
