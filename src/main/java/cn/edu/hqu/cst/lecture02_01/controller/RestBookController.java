package cn.edu.hqu.cst.lecture02_01.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.edu.hqu.cst.lecture02_01.model.ApiResponse;
import cn.edu.hqu.cst.lecture02_01.model.Book;

@RestController
@RequestMapping("/restapi/books")
public class RestBookController {


    // 获取所有图书
    @GetMapping("/all")
    public ApiResponse<List<Book>> getAllBooks() {
        List<Book> books=new ArrayList<Book>();
    	books.add(new Book("西游记","吴承恩"));
    	books.add(new Book("红楼梦","曹雪芹"));
        if(books.size()!=0) {
        	return ApiResponse.success(books);
        }else {
        	return ApiResponse.error("没有查到任何书籍!");
        }
    }

    // 根据ID获取图书
    @GetMapping("/{id}")
    public ApiResponse<Book> getBookById(@PathVariable Long id) {
    	return null;
    }

    // 添加新图书
    @PostMapping
    public ApiResponse<Book> createBook(@RequestBody Book book) {
        return null;
    }

    // 更新图书信息
    @PutMapping("/{id}")
    public ApiResponse<Book> updateBook(@PathVariable Long id, @RequestBody Book book) {
        return ApiResponse.success(book);
    }

    // 删除图书
    @DeleteMapping("/{id}")
    public ApiResponse<Void> deleteBook(@PathVariable Long id) {
        return null;
    }
}

