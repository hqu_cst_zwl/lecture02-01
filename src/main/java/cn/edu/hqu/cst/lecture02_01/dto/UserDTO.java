package cn.edu.hqu.cst.lecture02_01.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class UserDTO {
	@NotNull(message="不能为空")
	private Long id;
	@Size(min=5,max=30)
	private String userName;
	@Email
	private String email;
}
