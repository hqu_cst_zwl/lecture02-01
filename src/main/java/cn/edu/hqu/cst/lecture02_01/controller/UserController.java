package cn.edu.hqu.cst.lecture02_01.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import cn.edu.hqu.cst.lecture02_01.dto.UserDTO;
import jakarta.validation.Valid;

@RestController
public class UserController {
	@PostMapping("/users")
	public ResponseEntity<String> createUser(@Valid @RequestBody UserDTO user){
		return ResponseEntity.ok("用户创建成功");
	}

}
