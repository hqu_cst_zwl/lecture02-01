package cn.edu.hqu.cst.lecture02_01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
@OpenAPIDefinition(info=@Info(title="API",version="v1",description="API描述"))
@SpringBootApplication
public class Lecture0216Application {

	public static void main(String[] args) {
		SpringApplication.run(Lecture0216Application.class, args);
	}

}
