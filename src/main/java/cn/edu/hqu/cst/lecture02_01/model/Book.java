package cn.edu.hqu.cst.lecture02_01.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Book {
	private String title;
	private String author;
}
